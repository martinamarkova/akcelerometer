/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Akcelerometer.cpp
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "MMA8451Q/MMA8451Q.h"
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include "fsl_i2c.h"
#include "fsl_pit.h"
#include "math.h"

/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */

volatile bool pitFlag = false;

void boardInit() {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
}

void ledInit() {
    //LED_INIT();
    LED_RED_INIT(kGPIO_DigitalOutput);
    LED_GREEN_INIT(kGPIO_DigitalOutput);
    LED_BLUE_INIT(kGPIO_DigitalOutput);
}

void biela() {
	LED_GREEN_ON();
	LED_RED_ON();
	LED_BLUE_ON();
}

void pauza() {
	int a = 500000;
	while(a>0) {
		a--;
	}
}

/*
 * dolno-pripustny filter
 * 𝑔(𝑛) = (1 − alfa) × g(𝑛 − 1) + alfa × 𝑟𝑎𝑤𝑋𝑌z
* alfa = (2*pi*deltaT*fc)/(2*pi*deltaT*fc+1)
 *
 */

void hornoPriepustnyFilter(float* xyzHP, float* xyzPred, float* xyzAktualny) {
	float pi = 3.14159265;
	float fc = 0.2;
	float T = 0.1; //vzorkovacia frekvencia

	float alfa = 1/(2*pi*T*fc + 1);
	xyzHP[0] = alfa*xyzHP[0] + (alfa*(xyzAktualny[0] - xyzPred[0]));
	xyzHP[1] = alfa*xyzHP[1] + (alfa*(xyzAktualny[1] - xyzPred[1]));
	xyzHP[2] = alfa*xyzHP[2] + (alfa*(xyzAktualny[2] - xyzPred[2]));
	xyzPred[0] = xyzAktualny[0];
	xyzPred[1] = xyzAktualny[1];
	xyzPred[2] = xyzAktualny[2];
}

void dolnoPriepustnyFilter(float* xyzDP, float* xyzAktualny) {
	float pi = 3.14159265;
	float fc = 0.2;
	float T = 0.1; //vzorkovacia frekvencia

	float alfa = (2*pi*T*fc)/(1+2*pi*T*fc);
	xyzDP[0] = alfa*xyzAktualny[0] + (1-alfa)*(xyzDP[0]);
	xyzDP[1] = alfa*xyzAktualny[1] + (1-alfa)*(xyzDP[1]);
	xyzDP[2] = alfa*xyzAktualny[2] + (1-alfa)*(xyzDP[2]);
}

void getNaklonXY(float* xyzDP, int* naklonXYp) {
	int naklonX, naklonY = 0;
	naklonX = round( atan(xyzDP[0] / pow( pow(xyzDP[1],2) + pow(xyzDP[2],2), 0.5)) * (180/3.14));
	naklonY = round( atan(xyzDP[1] / pow( pow(xyzDP[0],2) + pow(xyzDP[2],2), 0.5)) * (180/3.14));
	naklonXYp[0] = naklonX;
	naklonXYp[1] = naklonY;
}

extern "C" void PIT_IRQHandler(void) {
	PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag);
	pitFlag = true;
}

void myPit() {
	/* Structure of initialize PIT */
	pit_config_t myPitCon;

	PIT_GetDefaultConfig(&myPitCon);
	/* Init pit module */
	PIT_Init(PIT, &myPitCon);
	/* Set timer period for channel 0 */
	PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(100000, CLOCK_GetFreq(kCLOCK_BusClk)));
	//PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(100000, 24000000));
	/* Enable timer interrupts for channel 0 */
	PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
	/* Enable at the NVIC */
	EnableIRQ(PIT_IRQn);
	/* Start channel 0 */
	PIT_StartTimer(PIT, kPIT_Chnl_0);
}

int main(void) {
	float xyzPred[3], xyzHP[3], xyzDP[3], osxyzAktual[3];
	int naklonXY[2];

	boardInit();
	//ledInit();

    //PRINTF("Hello World\n");
	myPit();
	MMA8451Q akcelerometer(0x1D);

    while(1) {
    	if(pitFlag) {
    		akcelerometer.getAccAllAxis(osxyzAktual);
    		PRINTF("akcelerometer os x,y,z bez filtra\n\r");
    		PRINTF("os x : %d\n\r", (int) (osxyzAktual[0]*100));
    		PRINTF("os y : %d\n\r", (int) (osxyzAktual[1]*100));
    		PRINTF("os z : %d\n\r**********************************\n\r", (int) (osxyzAktual[2]*100));

    		hornoPriepustnyFilter(xyzHP, xyzPred, osxyzAktual);
    		PRINTF("akcelerometer: horno priepustny filter\n\r");
    		PRINTF("os x : %d\n\r", (int) (xyzHP[0]*100));
    		PRINTF("os y : %d\n\r", (int) (xyzHP[1]*100));
    		PRINTF("os z : %d\n\r**********************************\n\r", (int) (xyzHP[2]*100));

    		dolnoPriepustnyFilter(xyzDP, osxyzAktual);
    		PRINTF("akcelerometer: dolno priepustny filter\n\r");
    		PRINTF("os x : %d\n\r", (int) (xyzDP[0]*100));
    		PRINTF("os y : %d\n\r", (int) (xyzDP[1]*100));
    		PRINTF("os z : %d\n\r**********************************\n\r", (int) (xyzDP[2]*100));

    		getNaklonXY(xyzDP, naklonXY);
    		PRINTF("akcelerometer: naklon XY\n\r");
    		if(naklonXY[0] < 0) {
    			PRINTF("naklon x : -%d\n\r", naklonXY[0]);
			}else PRINTF("naklon x : %d\n\r", naklonXY[0]);

    		if(naklonXY[1] < 0) {
    			PRINTF("naklon y : -%d\n\r", naklonXY[1]);
			} else PRINTF("naklon y : %d\n\r", naklonXY[1]);

    		//biela();
    		//LED_GREEN_ON();
    		//pauza();
    		//LED_GREEN_TOGGLE();
    		pitFlag = false;
    	}
    }
    return 0 ;
}
